import { Player } from '@prisma/client';
import { CompleteScene, CompleteSession, actionsTypes } from '../services/types';

export class GameInfo {
	private player?: Player;
	private session?: CompleteSession;
	private end: boolean = false;
	private nextElementScript?: CompleteScene;

	async setSession(session: CompleteSession) {
		this.session = session;
	}

	setPlayer(player: Player) {
		this.player = player;
	}

	async setNextElementScript(nextElementScript: CompleteScene) {
		if (nextElementScript.id === actionsTypes.sq) {
			return this.setEnd();
		}

		this.nextElementScript = nextElementScript;
	}

	getSession() {
		return this.session;
	}

	getPlayer() {
		return this.player;
	}

	getNextScript() {
		return this.nextElementScript;
	}

	setEnd() {
		this.end = true;
	}

	hasEnded() {
		return this.end;
	}
}
