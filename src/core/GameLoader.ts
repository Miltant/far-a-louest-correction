import { CanCreateSceneModel } from '../models/factory/IModelFactory';
import { CanGetDefaultScene } from '../models/scene/IScene';
import { GameInfo } from './GameInfo';
import { CompleteScene } from '../services/types';

export class GameLoader {
	private sceneModel: CanGetDefaultScene;

	constructor(modelFactory: CanCreateSceneModel) {
		this.sceneModel = modelFactory.createSceneModel();
	}

	async loadScene(gameState: GameInfo): Promise<CompleteScene> {
		const session = gameState.getSession();
		if (session && !session.scene) {
			const scene = await this.sceneModel.getDefaultScene();
			if (!scene) throw new Error('Default Scene does not exist');
			return scene;
		}

		if (session?.scene) return session.scene;

		throw new Error('No scene found');
	}
}
