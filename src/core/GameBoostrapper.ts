import { input, confirm } from '@inquirer/prompts';
import { IPlayer } from '../models/player/IPlayer';
import { ISession } from '../models/session/ISession';
import { GameInfo } from './GameInfo';
import { GameLoader } from './GameLoader';
import { IModelFactory } from '../models/factory/IModelFactory';
import { IScene } from '../models/scene/IScene';
import { CanDialog } from '../services/Narator/INarator';

export class GameBoostrapper {
	private gameInfo: GameInfo;
	private modelFactory: IModelFactory;
	private playerModel: IPlayer;
	private sessionModel: ISession;
	private sceneModel: IScene;
	private gameLoader: GameLoader;
	private narator: CanDialog;
	constructor(modelFactory: IModelFactory, narator: CanDialog) {
		this.modelFactory = modelFactory;
		this.playerModel = this.modelFactory.createPlayerModel();
		this.sessionModel = this.modelFactory.createSessionModel();
		this.sceneModel = this.modelFactory.createSceneModel();
		this.narator = narator;

		this.gameLoader = new GameLoader(this.modelFactory);
		this.gameInfo = new GameInfo();
	}

	async init() {
		await this.narator.dialog("Bienvenue dans l'aventure jeune aventurier !");
		await this.narator.dialog(
			"Avant de commencer j'aurais besoin de de quelques informations..."
		);

		const answer = await input(
			{ message: 'Entrez votre nom' },
			{
				clearPromptOnDone: true,
			}
		);
		console.clear();

		const user = await this.playerModel.getPlayer(answer);
		if (!user) {
			const user = await this.playerModel.createPlayer(answer);

			const session = await this.sessionModel.createPlayerSession(user.id);

			if (session) {
				await this.gameInfo.setSession(session);
				this.gameInfo.setNextElementScript(await this.gameLoader.loadScene(this.gameInfo));
			}

			this.gameInfo.setPlayer(user);

			await this.narator.dialog(`Ah heureux de vous rencontrer ${answer}!`);
			await this.narator.dialog(
				`Une grande aventure nous attend, j'espère que vous êtes prêts.`
			);
			return this.gameInfo;
		}

		await this.narator.dialog(`Hum... il semblerait que votre tête me soit familière... !`);
		const continueSession = await confirm(
			{
				message: 'Une session existe déjà pour vous, voulez vous continuer la précédente ?',
			},
			{
				clearPromptOnDone: true,
			}
		);

		this.gameInfo.setPlayer(user);

		if (continueSession) {
			await this.narator.dialog('Très bien continuons cette aventure dans ce cas !');
			const session = await this.sessionModel.getLastPlayerSession(user.id);
			if (!session) {
				await this.sessionModel.createPlayerSession(user.id);
			}

			if (session) {
				await this.gameInfo.setSession(session);
				if (session.scene)
					this.gameInfo.setNextElementScript(
						await this.gameLoader.loadScene(this.gameInfo)
					);
			}

			return this.gameInfo;
		}

		await this.narator.dialog(
			"Repartons alors de zero... allez savoir ce qu'il va advenir de vous cette fois !"
		);
		const session = await this.sessionModel.createPlayerSession(user.id);

		await this.gameInfo.setSession(session);
		this.gameInfo.setNextElementScript(await this.gameLoader.loadScene(this.gameInfo));
		return this.gameInfo;
	}
}
