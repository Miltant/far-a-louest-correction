import { CompleteSession } from '../../services/types';

export type ISession = CanGetLastSession & CanCreatePlayerSession & CanUpdateSessionProgress;

export interface CanGetLastSession {
	getLastPlayerSession(userId: string): Promise<CompleteSession | null>;
}

export interface CanCreatePlayerSession {
	createPlayerSession(userId: string): Promise<CompleteSession>;
}

export interface CanUpdateSessionProgress {
	updateSessionProgress(session: CompleteSession, sceneId: string): void;
}
