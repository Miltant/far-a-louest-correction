import { PrismaClient } from '@prisma/client';
import { CanCreatePlayerSession, CanGetLastSession, CanUpdateSessionProgress } from './ISession';
import { CompleteSession } from '../../services/types';
import { IsConnectable } from '../../services/db/IDb';

export class SessionModel
	implements CanCreatePlayerSession, CanGetLastSession, CanUpdateSessionProgress
{
	private idGenerator: () => string;
	private db: PrismaClient;

	constructor(db: IsConnectable, idGenerator: () => string) {
		this.idGenerator = idGenerator;
		this.db = db.connect() as PrismaClient;
	}

	async createPlayerSession(userId: string) {
		return await this.db.session.create({
			data: {
				player: {
					connect: {
						id: userId,
					},
				},
				sessionKey: this.idGenerator(),
			},
			include: {
				scene: {
					include: {
						dialogs: true,
						actions: {
							include: {
								link: true,
							},
						},
					},
				},
			},
		});
	}

	async getLastPlayerSession(userId: string) {
		return await this.db.session.findFirst({
			where: {
				player: {
					id: userId,
				},
			},
			orderBy: {
				date: 'desc',
			},
			include: {
				scene: {
					include: {
						dialogs: true,
						actions: {
							include: {
								link: true,
							},
						},
					},
				},
			},
		});
	}

	async updateSessionProgress(session: CompleteSession, sceneId: string) {
		await this.db.session.update({
			where: {
				id: session.id,
			},
			data: {
				scene: {
					connect: {
						id: sceneId,
					},
				},
			},
		});
	}
}
