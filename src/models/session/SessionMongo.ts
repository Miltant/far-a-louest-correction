import { CanCreatePlayerSession, CanGetLastSession, CanUpdateSessionProgress } from './ISession';
import { CompleteSession } from '../../services/types';
import { IsConnectable } from '../../services/db/IDb';
import { MongooseModel } from '../../services/db/MongooseConnector';

export class SessionModelMongo
	implements CanCreatePlayerSession, CanGetLastSession, CanUpdateSessionProgress
{
	private idGenerator: () => string;
	private db: MongooseModel;

	constructor(db: IsConnectable, idGenerator: () => string) {
		this.idGenerator = idGenerator;
		this.db = db.connect() as MongooseModel;
	}

	async createPlayerSession(userId: string) {
		await this.db.Player.updateOne(
			{
				_id: userId,
			},
			{
				$push: {
					Session: {
						sessionKey: this.idGenerator(),
					},
				},
			}
		);
		const player = await this.db.Player.findOne({
			_id: userId,
		});

		return {
			id: player._id.toString(),
			sessionKey: player.Session[0].sessionKey,
			date: player.Session[0].date,
			playerId: player._id.toString(),
			sceneId: null,
			scene: null,
		};
	}

	async getLastPlayerSession(userId: string) {
		const db = await this.db;
		const player = await this.db.Player.findOne({
			_id: userId,
		});

		const last = player.Session[player.Session.length - 1];

		if (!last) return null;

		const scene = await this.db.Scene.findOne({
			name: last.scene,
		});

		if (!scene) return null;

		const sceneObject = scene.toObject();

		return {
			id: player._id.toString(),
			sessionKey: last.sessionKey,
			date: last.date,
			playerId: player._id.toString(),
			sceneId: sceneObject.name,
			scene: {
				...sceneObject,
				id: sceneObject.name,
				actions: sceneObject.actions.map(
					(action: { type: string; link: string; content: string }) => ({
						...action,
						link: {
							id: action.link,
							type: action.type,
						},
					})
				),
			},
		};
	}

	async updateSessionProgress(session: CompleteSession, sceneId: string) {
		const player = await this.db.Player.findOne({
			_id: session.id,
		});

		if (!player) return;

		await this.db.Player.updateOne(
			{
				_id: session.id,
			},
			{
				$set: {
					[`Session.${player.Session.length - 1}.scene`]: sceneId,
					[`Session.${player.Session.length - 1}.date`]: new Date(),
				},
			}
		);
	}
}
