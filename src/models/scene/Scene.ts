import { PrismaClient } from '@prisma/client';
import { CanGetDefaultScene, CanGetScene } from './IScene';
import { IsConnectable } from '../../services/db/IDb';

export class SceneModel implements CanGetDefaultScene, CanGetScene {
	private db: PrismaClient;

	constructor(db: IsConnectable) {
		this.db = db.connect() as PrismaClient;
	}

	async getDefaultScene() {
		return this.getScene('default');
	}

	async getScene(scene: string) {
		return await this.db.scene.findUnique({
			where: {
				id: scene,
			},
			include: {
				dialogs: true,
				actions: {
					include: {
						link: true,
					},
				},
			},
		});
	}
}
