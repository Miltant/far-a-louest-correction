import { Player } from '@prisma/client';
export type IPlayer = canGetPlayer & canCreatePlayer;

export interface canGetPlayer {
	getPlayer(name: string): Promise<Player | null>;
}

export interface canCreatePlayer {
	createPlayer(name: string): Promise<Player>;
}
