import { IPlayer } from '../player/IPlayer';
import { IScene } from '../scene/IScene';
import { ISession } from '../session/ISession';

export type IModelFactory = CanCreatePlayerModel & CanCreateSceneModel & CanCreateSessionModel;

export interface CanCreatePlayerModel {
	createPlayerModel(): IPlayer;
}

export interface CanCreateSessionModel {
	createSessionModel(): ISession;
}

export interface CanCreateSceneModel {
	createSceneModel(): IScene;
}
