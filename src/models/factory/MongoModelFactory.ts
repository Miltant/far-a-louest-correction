import { IsConnectable } from '../../services/db/IDb';
import { MongooseConnector } from '../../services/db/MongooseConnector';
import { generateId } from '../../services/idGeneration';
import { IPlayer } from '../player/IPlayer';
import { PlayerModelMongo } from '../player/PlayerMongo';
import { SceneModelMongo } from '../scene/SceneMongo';
import { ISession } from '../session/ISession';
import { SessionModelMongo } from '../session/SessionMongo';
import { CanCreatePlayerModel, CanCreateSceneModel, CanCreateSessionModel } from './IModelFactory';

export class MongoModelFactory
	implements CanCreatePlayerModel, CanCreateSceneModel, CanCreateSessionModel
{
	private db: IsConnectable;
	constructor() {
		this.db = new MongooseConnector();
	}

	createPlayerModel(): IPlayer {
		return new PlayerModelMongo(this.db);
	}

	createSessionModel(): ISession {
		return new SessionModelMongo(this.db, generateId);
	}

	createSceneModel() {
		return new SceneModelMongo(this.db);
	}
}
