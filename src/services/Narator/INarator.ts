export interface CanDialog {
	dialog(text: string): Promise<void>;
}
