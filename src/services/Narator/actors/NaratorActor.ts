import chalk from 'chalk';
import { typeWriterEffect } from '../../typeWriterEffect';
import { CanSpeak } from './IActor';

export class NaratorActor implements CanSpeak {
	async speak(text: string) {
		await typeWriterEffect(text, 20, chalk.magenta);
	}
}
