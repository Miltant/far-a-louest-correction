import { Prisma } from '@prisma/client';

export const enum actionsTypes {
	msg = 'msg',
	room = 'room',
	action = 'action',
	sq = 'quit',
}

export type CompleteScene = Prisma.SceneGetPayload<{
	include: {
		dialogs: true;
		actions: {
			include: {
				link: true;
			};
		};
	};
}>;

export type CompleteSession = Prisma.SessionGetPayload<{
	include: {
		scene: {
			include: {
				dialogs: true;
				actions: {
					include: {
						link: true;
					};
				};
			};
		};
	};
}>;
