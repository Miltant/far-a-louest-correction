import { IsConnectable } from './IDb';
import mongoose, { Schema } from 'mongoose';

export type MongooseModel = { [key: string]: typeof mongoose.Model };

export class MongooseConnector implements IsConnectable {
	private models: { [key: string]: typeof mongoose.Model } = {};
	public constructor() {
		this.setup();
		mongoose.connect(process.env.MONGODB_URL as string);
	}

	private async setup() {
		const scene = new Schema({
			name: String,
			type: String,
			dialogs: {
				type: [Schema.Types.Mixed],
				default: [],
			},
			actions: {
				type: [Schema.Types.Mixed],
				default: [],
			},
		});

		const player = new Schema({
			name: String,
			Session: {
				type: [Schema.Types.Mixed],
				default: [],
			},
		});

		this.models.Scene = mongoose.model('Scene', scene);
		this.models.Player = mongoose.model('Player', player);
	}

	connect(): unknown {
		return this.models;
	}
}
