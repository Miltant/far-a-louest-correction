import { Narator } from './services/Narator/Narator';
import { SelfActor } from './services/Narator/actors/SelfActor';
import { PNJActor } from './services/Narator/actors/PNJActor';
import { GameBoostrapper } from './core/GameBoostrapper';
import { GameSaver } from './core/GameSaver';
import { SqlModelFactory } from './models/factory/SqlModelFactory';
import { NaratorActor } from './services/Narator/actors/NaratorActor';
import { GameEngine } from './core/GameEngine';
import { MongoModelFactory } from './models/factory/MongoModelFactory';

async function main() {
	try {
		console.clear();
		const modelFactory =
			process.env.DATABASE_TYPE === 'sql' ? new SqlModelFactory() : new MongoModelFactory();
		const bootstraper = new GameBoostrapper(modelFactory, new Narator(new NaratorActor()));
		const gameSaver = new GameSaver(modelFactory);

		const Engine = new GameEngine(
			bootstraper,
			modelFactory,
			{
				self: new Narator(new SelfActor()),
				pnj: new Narator(new PNJActor()),
				narrator: new Narator(new NaratorActor()),
			},
			gameSaver
		);

		await Engine.setup();
		await Engine.start();
	} catch (error) {
		console.error(error);
		process.exit(1);
	}
}

await main();
